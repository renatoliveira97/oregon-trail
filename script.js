class Traveler {
    constructor(name, food = 1, isHealthy = true) {
        this.name = name;
        this.food = food;
        this.isHealthy = isHealthy;
    }

    hunt () {
        this.food += 2;
    }

    eat () {
        if (this.food > 0) {
            this.food -= 1;
        } else {
            this.isHealthy = false;
        }
    }
}

class Wagon {
    constructor(capacity, passengers = []) {
        this.capacity = capacity;
        this.passengers = passengers;
    }

    getAvailableSeatCount() {
        return this.capacity - this.passengers.length;
    }

    join(passenger) {

        let totalDeAssentosVazios = this.getAvailableSeatCount();

        if (totalDeAssentosVazios > 0) {
            this.passengers.push(passenger);
        }

    }

    shouldQuarantine() {

        for (let i = 0; i < this.passengers.length; i++) {
            if (this.passengers[i].isHealthy === false) {
                return true;
            }
        }

        return false;

    }

    totalFood() {

        let foodCount = 0;

        for (let i = 0; i < this.passengers.length; i++) {
            foodCount += this.passengers[i].food;
        }

        return foodCount;

    }
}

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);